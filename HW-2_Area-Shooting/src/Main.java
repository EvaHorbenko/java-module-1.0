import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final int size = 5;
    private static final String blanc = "-";
    private static final String shoot = "*";
    private static final String kill = "X";

    private static final int empty = 0;
    private static final int touched = -1;
    private static final int goal = 1;

    private static boolean isWon = false;
    private static int[][] area = new int[size][size];


    private static final Scanner scanner = new Scanner(System.in);
    private static void print(String str) {
        System.out.print(str);
    }
    public static void main(String[] args) {
        setGoal();
        printArea();
        startGame();
    }

    private static void setGoal() {
        int row = getRandNumBelow(size - 1);
        int col = getRandNumBelow(size - 1);
        area[row][col] = goal;
//        print(String.format("row: %s, col: %s\n", row, col));
    }
    private static void printLine() {
        print("-");
        for (int i = 0; i < size + 1; i++) {
            print("----");
        }
        print("\n");
    }
    private static void printArea() {
        printLine();
        print("| 0 | 1 | 2 | 3 | 4 | 5 |\n");
        for (int i = 0; i < area.length; i++) {
            printLine();
            print(String.format("| %s |", (i + 1)));
            for (int j = 0; j < area[i].length; j++) {
                print(String.format(" %s |", getSign(area[i][j])));
            }
            print("\n");
        }
        printLine();
    }

    private static String getSign(int val) {
        if (isWon) {
            return val == empty ? blanc : val == touched ? shoot : kill;
        }
        return val == -1 ? shoot : blanc;
    }

    private static void startGame() {
        print("All set. Get ready to rumble!\n");
        while (!isWon) {
            int row;
            int col;

            try {
                print("Enter number of row: ");
                String lineRow = scanner.nextLine();
                row = Integer.parseInt(lineRow);
                print("\n");
            } catch (Exception e) {
                print("Enter numeric row value!\n");
                continue;
            }

            try {
                print("Enter number of column: ");
                String lineCol = scanner.nextLine();
                col = Integer.parseInt(lineCol);
                print("\n");
            } catch (Exception e) {
                print("Enter numeric column value!\n");
                continue;
            }

            try {
                if (area[row - 1][col - 1] == 1) {
                    isWon = true;
                } else {
                    area[row - 1][col - 1] = -1;
                }
            } catch (Exception e) {
                print(String.format("There are only %s rows and columns.\nEntered value was bigger.\n", size));
            }

            printArea();

        }

        print("You have won!");

    }

    private static int getRandNumBelow(int max) {
        return new Random().nextInt(max);
    }
}