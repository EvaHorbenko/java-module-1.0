import libs.Numbers;
import libs.Strings;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private static final String[] speciesList = {
            "собака", "кошка", "рыбка", "попугайчик", "хомяк"};
    private static final String[] namesList = {
            "Шелли", "Фредди", "Папичур", "Бен", "Трикси", "Редж",
    };
    private static final String[] habitsList = {
            "играть с игрушками",
            "играть с хозяином",
            "плавать", "кушать",
            "ловить солнечные зайчики",
            "бегать", "рыть", "спать", };
    private final String species;
    private final String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    private Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    static Pet of(String species, String nickname, int age, int trickLevel, String[] habits) {
        return new Pet(species, nickname, age, trickLevel, habits);
    }
    static Pet species_name(String species, String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(namesList)};
        return new Pet(species, nickname, 0, trickLvl, habit);
    }
    static Pet empty() {
        String species = Strings.getRandStrFromArr(speciesList);
        String name = Strings.getRandStrFromArr(namesList);
        return species_name(species, name);
    }

    public String getNickname() {
        return nickname;
    }
    public String getSpecies() {
        return species;
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }


    // species & nickName can't be changed after Pet creation
    // so, they are final & have no setters
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat () {
        Strings.print("Я кушаю!\n");
    }
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - %s. Я соскучился!\n", nickname));
    }
    public void foul() {
        Strings.print("Нужно хорошо замести следы...\n");
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + "'" +
                " , age=" + age +
                " , trickLevel=" + trickLevel +
                " , habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet pet)) return false;
        return getTrickLevel() == pet.getTrickLevel() &&
                getNickname().equals(pet.getNickname()) &&
                getSpecies().equals(pet.getSpecies());
    }
}
