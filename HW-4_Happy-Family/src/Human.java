import libs.Numbers;
import libs.Strings;

import java.util.Arrays;

public class Human {
    private final String name;
    private String surname;
    private final int year;
    private final int iq;
    private final String[][] schedule;
    //    private final String[][] schedule = new String[7][2];
    private Family family;

    private Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }
    static Human of(String name, String surname, int year, int iq, String[][] schedule) {
        return new Human(name, surname, year, iq, schedule);
    }
    static Human fullName_year(String name, String surname, int year) {
        int iq = Numbers.getRandNumBetween(0, 100);
        String[][] schedule = new String[2][7];
        return new Human(name, surname, year, iq, schedule);
    }
    static Human empty() {
        String name = Numbers.getRandNumBetween(0, 10) > 5 ? "Marry" : "George";
        String surname = Numbers.getRandNumBetween(0, 10) > 5 ? "Cooper" : "Sparks";
        int year = Numbers.getRandNumBetween(1930, 2023);
        int iq = Numbers.getRandNumBetween(0, 100);
        String[][] schedule = new String[2][7];
        return new Human(name, surname, year, iq, schedule);
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public String getSurname() {
        return surname;
    }


    public void greetPet() {
        Strings.print(String.format("Привет, %s", family.getPet().getNickname()));
    }
    public void describePet() {
        Strings.print(String.format(
                "У меня есть %s, ему %s лет, он %s.",
                family.getPet().getSpecies(), family.getPet().getAge(),
                family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"));
    }

    public boolean feedPet(boolean timeToFeed) {
        String message;
        boolean toFeed;
        String petName = family.getPet().getNickname();
        int trickLevel = family.getPet().getTrickLevel();

        toFeed = timeToFeed || trickLevel > Numbers.getRandNumBetween(0, 100);

        if (toFeed) {
            message = String.format("Хм... покормлю ка я %s.", petName);
        } else {
            message = String.format("Думаю, %s не голоден.", petName);
        }
        Strings.print(message);
        return toFeed;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human human)) return false;
        return iq == human.iq &&
                year == human.year &&
                name.equals(human.name) &&
                getSurname().equals(human.getSurname());
    }
}
