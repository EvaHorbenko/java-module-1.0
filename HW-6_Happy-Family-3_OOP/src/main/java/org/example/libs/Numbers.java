package org.example.libs;

import java.util.Random;

public class Numbers {
    public static int getRandNumBetween(int min, int max) {
        int diff = max - min;
        int random = new Random().nextInt(diff);
        int result = random + min;
        return result;
    }

}
