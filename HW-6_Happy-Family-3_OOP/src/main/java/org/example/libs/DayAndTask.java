package org.example.libs;

import org.example.enums.DayOfWeek;

public class DayAndTask {
    private static final String[] tasks = {"go to church", "drink bier in garden", "watch movie", "play computer-game", "learn", "go to work/school/uni", "go to forest", "drive car", "read book", "cook something tasty", "plant plants", "make something by hands"};
    private final DayOfWeek day;
    private final String task;

    private DayAndTask(DayOfWeek day, String task) {
        this.day = day;
        this.task = task;
    }
    public static DayAndTask of(DayOfWeek day, String task) {
        return new DayAndTask(day, task);
    }
    public static DayAndTask ofDay(DayOfWeek day) {
        String task = Strings.getRandStrFromArr(tasks);
        return new DayAndTask(day, task);
    }
    public static DayAndTask[] buildSchedule() {
        int length = DayOfWeek.values().length;
        DayAndTask[] schedule = new DayAndTask[length];
        for (int i = 0; i < DayOfWeek.values().length; i++) {
            // could use ".name()" on enum, like:
            // String day = DayOfWeek.values()[i].name();
            // but decide to not change type of argument
            // in "DayAndTask" constructor, from enum to string
            // because I find it not secure
            DayOfWeek day = DayOfWeek.values()[i];
            String task = Strings.getRandStrFromArr(tasks);
            schedule[i] = new DayAndTask(day, task);
        }
        return schedule;
    }

    @Override
    public String toString() {
        return String.format("{ %s: %s }", day, task);
    }
}
