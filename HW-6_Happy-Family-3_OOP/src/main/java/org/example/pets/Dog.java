package org.example.pets;

import org.example.enums.Species;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public class Dog extends Pet implements Foul {
    protected final Species species = Species.DOG;
    protected static String[] habitsList = new String[]{"играть с игрушками",
            "играть с хозяином", "плавать", "кушать",
            "ловить солнечные зайчики", "бегать", "рыть", "спать"};

    protected static String[] namesList = new String[]{"Шелли", "Бен", "Трикси", "Редж"};

    private Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    public static Dog of(String nickname, int age, int trickLevel, String[] habits) {
        return new Dog(nickname, age, trickLevel, habits);
    }
    public static Dog byName(String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(habitsList)};
        return new Dog(nickname, 0, trickLvl, habit);
    }
    public static Dog empty() {
        String name = Strings.getRandStrFromArr(namesList);
        return byName(name);
    }
    @Override
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - твой %s %s. Я соскучился!\n", "пес", nickname));
    }

    @Override
    public void foul() {
        Strings.print("Нужно хорошо замести следы...\nЭто не я лежал с грязными лапами на диване");
    }
}
