package org.example.pets;

import org.example.enums.Species;
import org.example.libs.Strings;

import java.util.Arrays;

public abstract class Pet {

    protected final Species species = Species.UNKNOWN;
    protected final String nickname;
    protected int age;
    protected int trickLevel;
    protected String[] habits;

    protected Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public String getNickname() {
        return nickname;
    }
    public Species getSpecies() {
        return species;
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }


    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat () {
        Strings.print("Я кушаю!\n");
    }
    public abstract void respond();

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + "'" +
                " , age=" + age +
                " , trickLevel=" + trickLevel +
                " , habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Pet pet)) return false;
        return getTrickLevel() == pet.getTrickLevel() &&
                getNickname().equals(pet.getNickname()) &&
                getSpecies().equals(pet.getSpecies());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Strings.print(String.format("%s will be deleted:\n%s\n", this.getClass().getSimpleName(), this));
    }
}
