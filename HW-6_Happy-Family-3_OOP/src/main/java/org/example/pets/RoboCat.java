package org.example.pets;

import org.example.enums.Species;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public class RoboCat extends Pet implements  Foul {
    protected final Species species = Species.ROBO_CAT;
    protected static String[] habitsList = new String[]{"играть с пылесосом",
            "играть с хозяином", "ездить", "кушать",
            "прыгать", "рыть", "заряжаться"};
    protected static String[] namesList = new String[]{"Робо", "РК", "РК-750", "Робо-912"};

    private RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    public static RoboCat of(String nickname, int age, int trickLevel, String[] habits) {
        return new RoboCat(nickname, age, trickLevel, habits);
    }
    public static RoboCat byName(String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(habitsList)};
        return new RoboCat(nickname, 0, trickLvl, habit);
    }
    public static RoboCat empty() {
        String name = Strings.getRandStrFromArr(namesList);
        return byName(name);
    }

    @Override
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - твой %s %s. Я соскучился!\n", "робо-кот", nickname));
    }

    @Override
    public void foul() {
        Strings.print("Нужно хорошо замести следы...\nРобо-кот тоже умеет делать пакости - я спрятал что-то");
    }
}
