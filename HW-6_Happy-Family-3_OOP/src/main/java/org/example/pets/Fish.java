package org.example.pets;

import org.example.enums.Species;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public class Fish extends Pet {
    protected final Species species = Species.FISH;
    protected static String[] habitsList = new String[]{
            "плавать в водорослях", "плавать по замку", "играть с хозяином"};
    protected static String[] namesList = new String[]{
            "Рыбка-0.0", "Рыбка-1.0", "Рыбка-2.0"};

    private Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    public static Fish of(String nickname, int age, int trickLevel, String[] habits) {
        return new Fish(nickname, age, trickLevel, habits);
    }
    public static Fish byName(String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(habitsList)};
        return new Fish(nickname, 0, trickLvl, habit);
    }
    public static Fish empty() {
        String name = Strings.getRandStrFromArr(namesList);
        return byName(name);
    }

    @Override
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - твой %s %s. Я соскучился!\nЖаль что ты меня не слышиш сквозь воу и стекло...", "робо-кот", nickname));
    }

}
