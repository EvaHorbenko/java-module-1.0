package org.example.pets;

import org.example.enums.Species;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public class DomesticCat extends Pet implements Foul {
    protected final Species species = Species.DOMESTIC_CAT;
    protected static String[] habitsList = new String[]{
            "играть с лазером", "лежать на солнце", "точить когти"};
    protected static String[] namesList = new String[]{"Авшора", "Кошка", "Кошка-2.0"};

    private DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    public static DomesticCat of(String nickname, int age, int trickLevel, String[] habits) {
        return new DomesticCat(nickname, age, trickLevel, habits);
    }
    public static DomesticCat byName(String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(habitsList)};
        return new DomesticCat(nickname, 0, trickLvl, habit);
    }
    public static DomesticCat empty() {
        String name = Strings.getRandStrFromArr(namesList);
        return byName(name);
    }

    @Override
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - твой %s %s. Я соскучился!\n", "кот", nickname));
    }

    @Override
    public void foul() {
        Strings.print("Нужно хорошо замести следы...\nэто не я поцарапал кресло...");
    }
}
