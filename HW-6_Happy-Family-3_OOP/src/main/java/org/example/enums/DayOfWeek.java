package org.example.enums;

import org.example.libs.Numbers;

public enum DayOfWeek {
    Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday;

    public static DayOfWeek getRandom() {
        int random = Numbers.getRandNumBetween(0, DayOfWeek.values().length);
        DayOfWeek[] daysOfWeek = values();
        return daysOfWeek[random];
    }
}
