package org.example.enums;

import org.example.libs.Numbers;

public enum Species {
    ROBO_CAT, DOMESTIC_CAT, DOG, FISH, UNKNOWN, PARROT, HAMSTER;

    public static Species getRandom() {
        int random = Numbers.getRandNumBetween(0, Species.values().length);
        Species[] species = values();
        return species[random];
    }
}
