package org.example.people;

import org.example.Family;
import org.example.libs.DayAndTask;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public abstract class Human {
    protected final String name;
    protected String surname;
    protected final int year;
    protected int iq;
    protected final DayAndTask[] schedule;
    protected Family family;

    protected Human(String name, String surname, int year, int iq, DayAndTask[] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public String getSurname() {
        return surname;
    }

    public void greetPet() {
        Strings.print(String.format("Привет, %s", family.getPet().getNickname()));
    }
    public void describePet() {
        Strings.print(String.format(
                "У меня есть %s, ему %s лет, он %s.",
                family.getPet().getSpecies(), family.getPet().getAge(),
                family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"));
    }

    public boolean feedPet(boolean timeToFeed) {
        String message;
        boolean toFeed;
        String petName = family.getPet().getNickname();
        int trickLevel = family.getPet().getTrickLevel();

        toFeed = timeToFeed || trickLevel > Numbers.getRandNumBetween(0, 100);

        if (toFeed) {
            message = String.format("Хм... покормлю ка я %s.", petName);
        } else {
            message = String.format("Думаю, %s не голоден.", petName);
        }
        Strings.print(message);
        return toFeed;
    }

    @Override
    public String toString() {
        String stringedSchedule = "";
        for (int i = 0; i < schedule.length; i++) {
            stringedSchedule = i == 0 ?
                    String.format("%s", schedule[i].toString()) :
                    String.format("%s, %s", stringedSchedule, schedule[i].toString());
        }
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + stringedSchedule +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Human human)) return false;
        return iq == human.iq &&
                year == human.year &&
                name.equals(human.name) &&
                getSurname().equals(human.getSurname());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Strings.print(String.format("Human will be deleted:\n%s\n",  this));
    }
}
