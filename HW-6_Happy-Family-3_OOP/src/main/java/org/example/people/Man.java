package org.example.people;

import org.example.libs.DayAndTask;
import org.example.libs.Numbers;
import org.example.libs.Strings;

public final class Man extends Human {
    private Man(String name, String surname, int year, int iq, DayAndTask[] schedule) {
        super(name, surname, year, iq, schedule);
    }
    public static Man of(String name, String surname, int year, int iq, DayAndTask[] schedule) {
        return new Man(name, surname, year, iq, schedule);
    }
    public static Man fullName_year(String name, String surname, int year) {
        int iq = Numbers.getRandNumBetween(0, 100);
        DayAndTask[] schedule = DayAndTask.buildSchedule();
        return new Man(name, surname, year, iq, schedule);
    }
    public static Man empty() {
        String name = "George";
        String surname = Numbers.getRandNumBetween(0, 10) > 5 ? "Cooper" : "Sparks";
        int year = Numbers.getRandNumBetween(1930, 2001);
        int iq = Numbers.getRandNumBetween(0, 100);
        DayAndTask[] schedule = DayAndTask.buildSchedule();
        return new Man(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        Strings.print(String.format("Привет, %s! Твой хозяин вернулся", family.getPet().getNickname()));
    }


    public void repairCar() {
        Strings.print("Пойду-ка я чинить машину");
    }
}
