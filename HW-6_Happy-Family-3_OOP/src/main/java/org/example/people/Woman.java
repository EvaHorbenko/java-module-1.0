package org.example.people;

import org.example.libs.DayAndTask;
import org.example.libs.Numbers;
import org.example.libs.Strings;

final public class Woman extends Human implements HumanCreator {
    private Woman(String name, String surname, int year, int iq, DayAndTask[] schedule) {
        super(name, surname, year, iq, schedule);
    }
    public static Woman of(String name, String surname, int year, int iq, DayAndTask[] schedule) {
        return new Woman(name, surname, year, iq, schedule);
    }
    public static Woman fullName_year(String name, String surname, int year) {
        int iq = Numbers.getRandNumBetween(0, 100);
        DayAndTask[] schedule = DayAndTask.buildSchedule();
        return new Woman(name, surname, year, iq, schedule);
    }
    public static Woman empty() {
        String name = "Marry";
        String surname = Numbers.getRandNumBetween(0, 10) > 5 ? "Cooper" : "Sparks";
        int year = Numbers.getRandNumBetween(1930, 2001);
        int iq = Numbers.getRandNumBetween(0, 100);
        DayAndTask[] schedule = DayAndTask.buildSchedule();
        return new Woman(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        Strings.print(String.format("Привет, %s! Твоя хозяйка вернулась.", family.getPet().getNickname()));
    }

    @Override
    public Human bornChild() {
        int rnd = Numbers.getRandNumBetween(0, 10);
        Human child = rnd > 5 ? Man.empty() : Woman.empty();
        child.family = this.family;
        Human father = this.family.getFather();
        child.surname = father.surname;
        child.iq = (int) ((this.iq + father.iq) / 2);
        return child;
    }

    public void makeUp() {
        Strings.print("Подкрашусь-ка я перед выходом");
    }
}
