import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Integer randNum;
    private static String name;
    private static final Scanner scanner = new Scanner(System.in);

    private static void print(String str) {
        System.out.print(str);
    }

    public static void main(String[] args) {
        randNum = getRandNum();
        name = getPlayerName();

        startGame();
    }

    private static Integer getRandNum() {
        return new Random().nextInt(100);
    }

    private static String getPlayerName() {
        print("Enter your name: ");
        return scanner.nextLine();
    }

    private static void startGame() {
        print("Let the game begin!\n");

        int number;
        String message;
        boolean isRight = false;
        while (!isRight) {
            print("Enter number: ");
            String line = scanner.nextLine();
            try {
                number = Integer.parseInt(line);
                if (number > randNum) {
                    message = "Your number is too big. Please, try again.\n";
                } else if (number < randNum) {
                    message = "Your number is too small. Please, try again.\n";
                } else {
                    isRight = true;
                    message = String.format("Congratulations, %s!", name);
                }
            } catch (NumberFormatException e) {
                message = "Entered value is not Integer\n";
            }
            print(message);
        }
        scanner.close();
    }
}