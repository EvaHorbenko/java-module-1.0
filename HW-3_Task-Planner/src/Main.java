import java.util.Scanner;

public class Main {
    public static String[][] schedule = {
            {"Sunday"   , "go to the forest with dog;"},
            {"Monday"   , "go to courses; watch a film;"},
            {"Tuesday"  , "learn german;"},
            {"Wednesday", "learn driving;"},
            {"Thursday" , "go to courses;"},
            {"Friday"   , "go to the Cinema;"},
            {"Saturday" , "go to courses; read a book"},
    };
    public static boolean askAgain = true;
    public static Scanner scanner = new Scanner(System.in);
    public static void print(String str) {
        System.out.print(str);
    }
    public static void main(String[] args) {
        while (askAgain) {
            print("Please, input the day of the week: ");
            String input = scanner.nextLine();

            if (input.toLowerCase().contains("exit")) {
                askAgain = false;
                break;
            }

            String message = "Sorry, I don't understand you, please try again.\n";
            boolean printMessage = true;
            for (int i = 0; i < schedule.length; i++) {
                if(input.trim().equalsIgnoreCase(schedule[i][0])) {
                    message = String.format("Your tasks for %s: %s.\n", schedule[i][0], schedule[i][1]);
                } else if(input.toLowerCase().contains("change") &&
                        input.toLowerCase().contains(schedule[i][0].toLowerCase())) {
                    print(String.format("Please, input new tasks for %s: ", schedule[i][0]));
                    schedule[i][1] = scanner.nextLine();
                    printMessage = false;
                }
            }

            if(printMessage) print(message);
        }
    }
}