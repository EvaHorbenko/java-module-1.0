import org.example.Family;
import org.example.Human;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class FamilyTest {
    static Family family;
    static Human[] children = new Human[3];
    @BeforeAll
    static void setUp() {
        family = Family.of(Human.empty(), Human.empty());
        for (int i = 0; i < children.length; i++) {
            children[i] = Human.empty();
            family.addChild(children[i]);
        }
    }
    @Test
    void testDeleteChildByHuman() {
        Human lastChild = family.getChildren()[children.length - 1];
        family.deleteChild(lastChild);
        Human[] actual = family.getChildren();
        Human[] expected = new Human[]{children[0], children[1]};
        Assertions.assertArrayEquals(actual, expected);
        Assertions.assertFalse(family.deleteChild(Human.empty()));
    }

    @Test
    void testDeleteChildByIdxLess() {
        Assertions.assertFalse(family.deleteChild(-1));
    }
    @Test
    void testDeleteChildByIdxMore() {
        int lastChildIdx = family.getChildren().length - 1;
        Assertions.assertFalse(family.deleteChild(lastChildIdx + 1));
    }
    @Test
    void testDeleteChildByIdx() {
        int lengthBefore = family.getChildren().length;
        family.deleteChild(1);
        int lengthAfter = family.getChildren().length;
        int expected = lengthBefore - 1;
        Assertions.assertTrue(lengthAfter == expected);
    }

    @Test
    void testAddChild() {
        int lengthBefore = family.getChildren().length;
        Human newChild = Human.empty();
        family.addChild(newChild);
        int lengthAfter = family.getChildren().length;
        Assertions.assertEquals(lengthAfter, lengthBefore + 1);
        Human lastChild = family.getChildren()[family.getChildren().length - 1];
        Assertions.assertEquals(newChild, lastChild);
    }

    @Test
    void testCountFamily() {
        for (int i = family.getChildren().length - 1; i >= 0 ; i--) {
            family.deleteChild(i);
        }
        Assertions.assertEquals(2, family.countFamily());
        family.addChild(Human.empty());
        Assertions.assertEquals(3, family.countFamily());
        family.deleteChild(0);
        Assertions.assertEquals(2, family.countFamily());
    }
}
