import org.example.Family;
import org.example.Human;
import org.example.Pet;
import org.example.enums.DayOfWeek;
import org.example.enums.Species;
import org.example.libs.DayAndTask;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ToStringTest {
    static Pet pet;
    static Human mother;
    static Human father;
    static Family family;

    @BeforeAll
    static void setUp() {
        pet = Pet.of(
                Species.DOG, "Shelly", 4, 97,
                new String[]{"catch sunbeams", "run in the forest", "sleep"});
        DayAndTask[] schedule = new DayAndTask[]{
                DayAndTask.of(DayOfWeek.Sunday, "Sunday staffs"),
                DayAndTask.of(DayOfWeek.Monday, "Monday staffs"),
                DayAndTask.of(DayOfWeek.Tuesday, "Tuesday staffs"),
                DayAndTask.of(DayOfWeek.Wednesday, "Wednesday staffs"),
                DayAndTask.of(DayOfWeek.Thursday, "Thursday staffs"),
                DayAndTask.of(DayOfWeek.Friday, "Friday staffs"),
                DayAndTask.of(DayOfWeek.Saturday, "Saturday staffs"),
        };
        mother = Human.of("Marry", "Cooper", 1960, 44, schedule);
        father = Human.of("George", "Cooper", 1955, 79, schedule);
        family = Family.of(mother, father);
    }

    @Test
    void testPet() {
        String petStr = "DOG{nickname='Shelly' , age=4 , trickLevel=97 , habits=[catch sunbeams, run in the forest, sleep]}";
        Assertions.assertEquals(petStr, pet.toString());
    }

    @Test
    void testHuman() {
        String motherStr = "Human{name='Marry', surname='Cooper', year=1960, iq=44, schedule={ Sunday: Sunday staffs }, { Monday: Monday staffs }, { Tuesday: Tuesday staffs }, { Wednesday: Wednesday staffs }, { Thursday: Thursday staffs }, { Friday: Friday staffs }, { Saturday: Saturday staffs }}";
        Assertions.assertEquals(motherStr, mother.toString());
    }

    @Test
    void testFamily() {
        String familyStr = "Family{mother=Human{name='Marry', surname='Cooper', year=1960, iq=44, schedule={ Sunday: Sunday staffs }, { Monday: Monday staffs }, { Tuesday: Tuesday staffs }, { Wednesday: Wednesday staffs }, { Thursday: Thursday staffs }, { Friday: Friday staffs }, { Saturday: Saturday staffs }}, father=Human{name='George', surname='Cooper', year=1955, iq=79, schedule={ Sunday: Sunday staffs }, { Monday: Monday staffs }, { Tuesday: Tuesday staffs }, { Wednesday: Wednesday staffs }, { Thursday: Thursday staffs }, { Friday: Friday staffs }, { Saturday: Saturday staffs }}, children=[], pet=}";
        Assertions.assertEquals(familyStr, family.toString());
    }

}
