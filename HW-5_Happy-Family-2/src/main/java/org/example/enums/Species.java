package org.example.enums;

import org.example.libs.Numbers;

public enum Species {
    CAT, DOG, FISH, PARROT, HAMSTER;

    public static Species getRandom() {
        int random = Numbers.getRandNumBetween(0, Species.values().length);
        Species[] species = values();
        return species[random];
    }
}
