package org.example;

import org.example.enums.DayOfWeek;
import org.example.enums.Species;
import org.example.libs.DayAndTask;
import org.example.libs.Strings;

public class Main {
    public static void main(String[] args) {

//        Strings.print(Human.empty().toString());

//        for (int i = 0; i < 1000000; i++) {
//            Human.empty();
//        }
        DayAndTask[] schedule = new DayAndTask[]{
                DayAndTask.of(DayOfWeek.Sunday, "Sunday staffs"),
                DayAndTask.of(DayOfWeek.Monday, "Monday staffs"),
                DayAndTask.of(DayOfWeek.Tuesday, "Tuesday staffs"),
                DayAndTask.of(DayOfWeek.Wednesday, "Wednesday staffs"),
                DayAndTask.of(DayOfWeek.Thursday, "Thursday staffs"),
                DayAndTask.of(DayOfWeek.Friday, "Friday staffs"),
                DayAndTask.of(DayOfWeek.Saturday, "Saturday staffs"),
        };
        Human mother = Human.of("Marry", "Cooper", 1960, 44, schedule);
        Human father = Human.of("George", "Cooper", 1955, 79, schedule);
        Family family = Family.of(mother, father);
        Strings.print(family.toString());
    }
}