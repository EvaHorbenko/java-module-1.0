package org.example;

import org.example.enums.Species;
import org.example.libs.Numbers;
import org.example.libs.Strings;

import java.util.Arrays;

public class Pet {
    private static final String[] speciesList = {
            "собака", "кошка", "рыбка", "попугайчик", "хомяк"};
    private static final String[] namesList = {
            "Шелли", "Фредди", "Папичур", "Бен", "Трикси", "Редж",
    };
    private static final String[] habitsList = {
            "играть с игрушками",
            "играть с хозяином",
            "плавать", "кушать",
            "ловить солнечные зайчики",
            "бегать", "рыть", "спать", };
    private final Species species;
    private final String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    private Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public static Pet of(Species species, String nickname, int age, int trickLevel, String[] habits) {
        return new Pet(species, nickname, age, trickLevel, habits);
    }
    public static Pet species_name(Species species, String nickname) {
        int trickLvl = Numbers.getRandNumBetween(0, 100);
        String[] habit = {Strings.getRandStrFromArr(habitsList)};
        return new Pet(species, nickname, 0, trickLvl, habit);
    }
    public static Pet empty() {
        Species species = Species.getRandom();
        String name = Strings.getRandStrFromArr(namesList);
        return species_name(species, name);
    }

    public String getNickname() {
        return nickname;
    }
    public Species getSpecies() {
        return species;
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }


    // species & nickName can't be changed after Pet creation
    // so, they are final & have no setters
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat () {
        Strings.print("Я кушаю!\n");
    }
    public void respond() {
        Strings.print(String.format("Привет, хозяин. Я - %s. Я соскучился!\n", nickname));
    }
    public void foul() {
        Strings.print("Нужно хорошо замести следы...\n");
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + "'" +
                " , age=" + age +
                " , trickLevel=" + trickLevel +
                " , habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Pet pet)) return false;
        return getTrickLevel() == pet.getTrickLevel() &&
                getNickname().equals(pet.getNickname()) &&
                getSpecies().equals(pet.getSpecies());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Strings.print(String.format("Pet will be deleted:\n%s\n",  this));
    }
}
