package org.example;

import org.example.libs.Strings;

import java.util.Arrays;

public class Family {
    private final Human mother;
    private final Human father;
    private Human[] children;
    private Pet pet;

    private Family(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.mother.setSurname(father.getSurname());
        this.father = father;
        this.father.setFamily(this);
        this.children = new Human[0];
    }
    public static Family of(Human mother, Human father) {
        return new Family(mother, father);
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children.clone();
    }

    public Pet getPet() {
        return pet;
    }

    public void addChild(Human child) {
        Human[] childrenUpd = Arrays.copyOf(children, children.length + 1);
        child.setFamily(this);
        child.setSurname(father.getSurname());
        childrenUpd[childrenUpd.length - 1] = child;
        children = childrenUpd;
    }
    public boolean deleteChild(int idx) {
        if (idx > children.length - 1 || idx < 0) return false;

        children[idx] = null;
        int count = 0;
        Human[] childrenUpd = new Human[children.length - 1];
        for (int i = 0; i < children.length; i++) {
            if (children[i] != null) {
                childrenUpd[count] = children[i];
                count++;
            }
        }
        children = childrenUpd;
        return true;
    }
    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                return deleteChild(i);
            }
        }
        return false;
    }
    public int countFamily() {
        return children.length + 2;
    }

    @Override
    public String toString() {
        Boolean hasPet = pet != null;
        String petStr = hasPet ? pet.toString() : "";
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + petStr +
                '}';
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Family family)) return false;
        return getMother().equals(family.getMother()) &&
                getFather().equals(family.getFather());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Strings.print(String.format("Family will be deleted:\n%s\n",  this));
    }
}
